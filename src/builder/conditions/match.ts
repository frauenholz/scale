/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    collection,
    pgettext,
    tripetto,
} from "tripetto";
import { ScaleOption } from "../option";

/** Assets */
import ICON from "../../../assets/icon.svg";
import ICON_UNANSWERED from "../../../assets/unanswered.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:match`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:scale", "Match scale answer");
    },
})
export class ScaleMatchCondition extends ConditionBlock {
    @affects("#name")
    @affects("#condition")
    @collection("#options")
    option?: ScaleOption;

    get icon() {
        return this.option ? ICON : ICON_UNANSWERED;
    }

    get name() {
        return (
            (this.option
                ? this.option.name
                : pgettext("block:scale", "Unanswered")) || this.type.label
        );
    }

    get title() {
        return this.name;
    }
}
