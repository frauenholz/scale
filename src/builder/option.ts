import {
    Collection,
    Forms,
    Slots,
    alias,
    definition,
    editor,
    insertVariable,
    isString,
    name,
    pgettext,
    score,
} from "tripetto";
import { Scale } from "./";

export class ScaleOption extends Collection.Item<Scale> {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:scale", "Name"),
            form: {
                title: pgettext("block:scale", "Option name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.group(pgettext("block:scale", "Options"));
        this.editor.option({
            name: pgettext("block:scale", "Identifier"),
            form: {
                title: pgettext("block:scale", "Option identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined, "")
                    ),

                    new Forms.Static(
                        pgettext(
                            "block:scale",
                            // tslint:disable-next-line:max-line-length
                            "If an option identifier is set, this identifier will be used as selected option value instead of the option label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:scale", "Score"),
            form: {
                title: pgettext("block:scale", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
