export interface IScaleOption {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
    readonly score?: number;
}

export interface IScale {
    readonly mode: "numeric" | "options";
    readonly options?: IScaleOption[];
    readonly from?: number;
    readonly to?: number;
    readonly stepSize?: number;
    readonly labelLeft?: string;
    readonly labelCenter?: string;
    readonly labelRight?: string;
    readonly justify?: boolean;
}
