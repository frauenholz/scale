/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:compare`,
})
export class ScaleCompareCondition extends ConditionBlock<{
    readonly mode: TMode;
    readonly value?: number | string;
    readonly to?: number | string;
}> {
    private getValue(slot: Slots.Slot, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Number) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return isNumberFinite(value) ? value : undefined;
    }

    @condition
    compare(): boolean {
        const scaleSlot = this.valueOf<number>();

        if (scaleSlot) {
            const value = this.getValue(scaleSlot.slot, this.props.value);

            switch (this.props.mode) {
                case "equal":
                    return (
                        (scaleSlot.hasValue ? scaleSlot.value : undefined) ===
                        value
                    );
                case "not-equal":
                    return (
                        (scaleSlot.hasValue ? scaleSlot.value : undefined) !==
                        value
                    );
                case "below":
                    return (
                        isNumberFinite(value) &&
                        scaleSlot.hasValue &&
                        scaleSlot.value < value
                    );
                case "above":
                    return (
                        isNumberFinite(value) &&
                        scaleSlot.hasValue &&
                        scaleSlot.value > value
                    );
                case "between":
                case "not-between":
                    const to = this.getValue(scaleSlot.slot, this.props.to);

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (scaleSlot.hasValue &&
                            scaleSlot.value >= Num.min(value, to) &&
                            scaleSlot.value <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
                case "defined":
                    return scaleSlot.hasValue;
                case "undefined":
                    return !scaleSlot.hasValue;
            }
        }

        return false;
    }
}
