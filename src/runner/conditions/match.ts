/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:match`,
})
export class ScaleMatchCondition extends ConditionBlock<{
    readonly option?: string;
}> {
    @condition
    match(): boolean {
        const scaleSlot = this.valueOf<string>();

        return (
            (scaleSlot && this.props.option === scaleSlot.reference) || false
        );
    }
}
