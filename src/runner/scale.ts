/** Dependencies */
import {
    NodeBlock,
    Num,
    Slots,
    assert,
    castToNumber,
    findFirst,
    markdownifyToString,
} from "tripetto-runner-foundation";
import { IScale } from "./interface";
import { MAX, MAX_DEFAULT, MIN, MIN_DEFAULT } from "./range";

export abstract class Scale extends NodeBlock<IScale> {
    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains the scale slot. */
    readonly scaleSlot = assert(
        this.valueOf<string | number, Slots.String | Slots.Number>(
            "scale",
            "static",
            {
                confirm: true,
                modifier: (data) => {
                    if (this.props.mode === "options" && data.value) {
                        if (!data.reference) {
                            const selected =
                                findFirst(
                                    this.props.options,
                                    (option) => option.value === data.value
                                ) ||
                                findFirst(
                                    this.props.options,
                                    (option) => option.id === data.value
                                ) ||
                                findFirst(
                                    this.props.options,
                                    (option) => option.name === data.value
                                ) ||
                                findFirst(
                                    this.props.options,
                                    (option) =>
                                        option.name.toLowerCase() ===
                                        data.string.toLowerCase()
                                );

                            return {
                                value:
                                    selected &&
                                    (selected.value || selected.name),
                                reference: selected && selected.id,
                            };
                        } else if (
                            !findFirst(
                                this.props.options,
                                (option) => option.id === data.reference
                            )
                        ) {
                            return {
                                value: undefined,
                                reference: undefined,
                            };
                        }
                    }
                },
                onChange: (slot) => {
                    if (this.scoreSlot) {
                        const selected = findFirst(
                            this.props.options,
                            (option) => option.id === slot.reference
                        );

                        this.scoreSlot.set(selected && (selected?.score || 0));
                    }
                },
            }
        )
    );

    /** Contains if the scale is required. */
    readonly required = this.scaleSlot.slot.required || false;

    get from(): number {
        return Num.range(
            Num.min(
                castToNumber(this.props.from, MIN_DEFAULT),
                castToNumber(this.props.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );
    }

    get to(): number {
        return Num.range(
            Num.max(
                castToNumber(this.props.from, MIN_DEFAULT),
                castToNumber(this.props.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );
    }

    get stepSize(): number {
        return Num.max(castToNumber(this.props.stepSize, MIN_DEFAULT), 1);
    }

    get options() {
        return this.props.mode === "options"
            ? this.props.options?.map((option) => ({
                  ...option,
                  name: markdownifyToString(option.name, this.context),
              })) || []
            : {
                  from: this.from,
                  to: this.to,
                  stepSize: this.stepSize,
              };
    }
}
